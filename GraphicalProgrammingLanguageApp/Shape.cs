﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    abstract class Shape : Shapes
    {
        //colour and positions of the shape, available to derived classes
        protected Color colour;
        protected int x, y;

        //default constructor
        public Shape()
        {
            colour = Color.Red;
            x = y = 100;
        }

        //contstructor with parameters
        public Shape(Color colour, int x, int y)
        {
            this.colour = colour;
            this.x = x;
            this.y = y;
        }

        //derived class need to implement these methods
        public abstract double calcArea();
        public abstract double calcPerimeter();
        public abstract void draw(Graphics g);
        
        //derived classes should change this method as needed
        public virtual void set(Color colour, params int[] list)
        {
            this.colour = colour;
            this.x = list[0];
            this.y = list[1];

        }

        public override string ToString()
        {
            return base.ToString() + ""+this.x+","+this.y+":";
        }
    }
}

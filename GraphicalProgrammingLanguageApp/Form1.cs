﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    public partial class Form1 : Form
    {



        //necessary code
        Bitmap DisplayBitmap = new Bitmap(640, 480);
        Canvass ShapeCanvass;
        String thecommands;
        String[] param;
        int[] intParam;
        int number;
        



        public Form1()
        {
            //form starts here
            InitializeComponent();
            ShapeCanvass = new Canvass(Graphics.FromImage(DisplayBitmap));
            Rectangle Rectangle = new Rectangle();




        }



        private void display_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics; //graphics event in
            g.DrawImageUnscaled(DisplayBitmap, 0, 0); //graphics event out


        }



        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            
            //Code for button press goes here, this is executed when Enter is pressed down
            if (e.KeyCode == Keys.Enter)
            {

               
                //brings in text so it can be used for commands
                String Command = commandLine.Text; 
                String trimmedInput = Command.ToLower().Trim();
                String[] inputArray = trimmedInput.Split();

               
                

                //checks array
                    for (int i = 0; i < inputArray.Length; i++)
                    {
                        Console.WriteLine("input: " + inputArray[i]);


                    }

                   
                     //separates first command for isntructions
                    thecommands = inputArray[0];
                if (commandLine.Text.Equals("clear"))
                {
                    Console.WriteLine("clear"); 
                    ShapeCanvass.Clear(); //clears canvas

                }
                else if (commandLine.Text.Equals("reset"))
                {
                    Console.WriteLine("reset");
                    ShapeCanvass.PenReset(); //resets pen position
                }
                else if (commandLine.Text.Equals("run"))
                {
                    Console.WriteLine("runs the command box");
                    buttonRun_Click(sender, null);

                }

                else if (thecommands.Equals("pen"))
                {
                    if (inputArray[1].Equals("red"))
                    {
                        Console.WriteLine("red pen");
                        ShapeCanvass.PenRed(); //makes pen red
                    }
                    else if (inputArray[1].Equals("green"))
                    {
                        Console.WriteLine("green pen");
                        ShapeCanvass.PenGreen(); //makes pen green
                    }
                    else if (inputArray[1].Equals("blue"))
                    {
                        Console.WriteLine("blue pen");
                        ShapeCanvass.PenBlue(); //makes pen blue
                    }
                    else
                    {
                        Console.WriteLine("Invalid pen"); //indicates error, further output can be added here
                    }
                }
                else if (thecommands.Equals("fill"))
                {
                    if (inputArray[1].Equals("on"))
                    {
                        Console.WriteLine("fill on");
                        ShapeCanvass.FillSwitch(true); //activates brush to fill in shapes
                    }
                    else if (inputArray[1].Equals("off"))
                    {
                        Console.WriteLine("fill off");
                        ShapeCanvass.FillSwitch(false); //deactivates brush
                    }

                }

                else if (thecommands.Equals("circle") || (thecommands.Equals("square")))
                    try
                    {

                        {
                            number = int.Parse(inputArray[1]);  //separates coordinates
                            Console.WriteLine(" 2 commands");
                        }
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc.Message);  //exception, can be imprived to be more precise 
                    }
                else
                    try
                    {
                        {
                            Console.WriteLine("commands: " + thecommands);
                            param = inputArray[1].Split(',', '.');        //separates coords
                            intParam = Array.ConvertAll(param, int.Parse);

                        }
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc.Message); //exception, can be imprived to be more precise 
                    }
                   
              
                //use instructions
                if (thecommands.Equals("drawto") && intParam.Length >= 2)
                {
                    ShapeCanvass.DrawLine(intParam[0], intParam[1]);
                    Console.WriteLine("Line");
                }
                else if(thecommands.Equals("moveto") && intParam.Length >= 2)
                {
                    
                    ShapeCanvass.MoveLine(intParam[0], intParam[1]);
                    Console.WriteLine("moved");
                }
                else if (thecommands.Equals("circle") == true)
                {
                    ShapeCanvass.DrawCircle(number);
                    Console.WriteLine("circle");
                }
                else if (thecommands.Equals("square") == true)
                {
                    ShapeCanvass.DrawSquare(number);
                    Console.WriteLine("square");
                }
                else if (thecommands.Equals("rect") && intParam.Length >= 2)
                {
                    ShapeCanvass.DrawRectangle(intParam[0], intParam[1]);
                    Console.WriteLine("square");
                }

                commandLine.Text = ""; //clears text field
                Refresh(); //Updates text field



            }

        }

        private void commandBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void display_Click(object sender, EventArgs e)
        {

        }

        private void commandOutput_Click(object sender, EventArgs e)
        {

        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            String[] lines = commandBox.Text.Split('\n');


            for (int i = 0; i < lines.Length; i++)
            {






                //brings in text so it can be used for commands
                String Command = lines[i];



                String trimmedInput = Command.ToLower().Trim();
                String[] inputArray = trimmedInput.Split();




                //separates first command for isntructions
                thecommands = inputArray[0];
                if (commandLine.Text.Equals("clear"))
                {
                    Console.WriteLine("clear");
                    ShapeCanvass.Clear(); //clears canvas

                }
                else if (commandLine.Text.Equals("reset"))
                {
                    Console.WriteLine("reset");
                    ShapeCanvass.PenReset(); //resets pen position
                }
                else if (thecommands.Equals("pen"))
                {
                    if (inputArray[1].Equals("red"))
                    {
                        Console.WriteLine("red pen");
                        ShapeCanvass.PenRed(); //makes pen red
                    }
                    else if (inputArray[1].Equals("green"))
                    {
                        Console.WriteLine("green pen");
                        ShapeCanvass.PenGreen(); //makes pen green
                    }
                    else if (inputArray[1].Equals("blue"))
                    {
                        Console.WriteLine("blue pen");
                        ShapeCanvass.PenBlue(); //makes pen blue
                    }
                    else
                    {
                        Console.WriteLine("Invalid pen"); //indicates error, further output can be added here
                    }
                }
                else if (thecommands.Equals("fill"))
                {
                    if (inputArray[1].Equals("on"))
                    {
                        Console.WriteLine("fill on");
                        ShapeCanvass.FillSwitch(true); //activates brush to fill in shapes
                    }
                    else if (inputArray[1].Equals("off"))
                    {
                        Console.WriteLine("fill off");
                        ShapeCanvass.FillSwitch(false); //deactivates brush
                    }

                }

                else if (thecommands.Equals("circle") || (thecommands.Equals("square")))
                    try
                    {

                        {
                            number = int.Parse(inputArray[1]);  //separates coordinates
                            Console.WriteLine(" 2 commands");
                        }
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc.Message);  //exception, can be imprived to be more precise 
                    }
                else
                    try
                    {
                        {
                            Console.WriteLine("commands: " + thecommands);
                            param = inputArray[1].Split(',', '.');        //separates coords
                            intParam = Array.ConvertAll(param, int.Parse);

                        }
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc.Message); //exception, can be imprived to be more precise 
                    }


                //use instructions
                if (thecommands.Equals("drawto") && intParam.Length >= 2)
                {
                    ShapeCanvass.DrawLine(intParam[0], intParam[1]);
                    Console.WriteLine("Line");
                }
                else if (thecommands.Equals("moveto") && intParam.Length >= 2)
                {

                    ShapeCanvass.MoveLine(intParam[0], intParam[1]);
                    Console.WriteLine("moved");
                }
                else if (thecommands.Equals("circle") == true)
                {
                    ShapeCanvass.DrawCircle(number);
                    Console.WriteLine("circle");
                }
                else if (thecommands.Equals("square") == true)
                {
                    ShapeCanvass.DrawSquare(number);
                    Console.WriteLine("square");
                }
                else if (thecommands.Equals("rect") && intParam.Length >= 2)
                {
                    ShapeCanvass.DrawRectangle(intParam[0], intParam[1]);
                    Console.WriteLine("square");
                }

                commandLine.Text = ""; //clears text field
                Refresh(); //Updates text field


            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DisplayBitmap.Save(@"C:\Users\nickk\OneDrive - Leeds Beckett University\Advanced Software Engineering\source\repos\GraphicalProgrammingLanguageApp\image.bmp");
        }
    }
}

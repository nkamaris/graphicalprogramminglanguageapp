﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    class Canvass
    {
        Graphics g;
        Pen Pen;
        SolidBrush Brush;
        int xPos, yPos; //saved current pen location
        Boolean fill; //fill shape on/off

        public Canvass(Graphics g) //constructor
        {
            //initialises code
            this.g = g;
            xPos = yPos = 0;
            Pen = new Pen(Color.Black, 1);
            Brush = new SolidBrush(Color.Black);
            fill = false;

        }

        public void PenRed() //changes pen and brush colour
        {
            Pen = new Pen(Color.Red, 1);
            Brush = new SolidBrush(Color.Red);
        }

        public void PenBlue() //changes pen and brush colour
        {
            Pen = new Pen(Color.Blue, 1);
            Brush = new SolidBrush(Color.Blue);
        }
        public void PenGreen() //changes pen and brush colour
        {
            Pen = new Pen(Color.Green, 1);
            Brush = new SolidBrush(Color.Green);
        }

        public void FillSwitch(Boolean toSwitch) //passes in boolean instruction
        {
            this.fill = toSwitch; //saves bollean instruction
        }
        public void DrawLine(int toX, int toY)
        {
            //Draw line and update Pen position
            g.DrawLine(Pen, xPos, yPos, toX, toY);
            xPos = toX;
            yPos = toY; 
        }

        public void MoveLine(int toX, int toY)
        {
            //moves pen without drawing
            xPos = toX;
            yPos = toY;
        }

        public void DrawCircle(int radius)
        {
            g.DrawEllipse(Pen, xPos,yPos, radius * 2, radius *2);
            if (fill == true) //if brush is activated then fill
            {
                g.FillEllipse(Brush, xPos, yPos, radius * 2, radius * 2);
            }
        }
        public void DrawSquare(int width)
        {
            g.DrawRectangle(Pen, xPos, yPos, xPos + width, yPos + width);
            if (fill == true) //if brush is activated then fill
            {
                g.FillRectangle(Brush, xPos, yPos, xPos + width, yPos + width);
            }

        }

        public void DrawRectangle(int width, int height)
        {
            g.DrawRectangle(Pen, xPos, yPos, xPos + width, yPos + height);
            if (fill == true) //if brush is activated then fill
            {
                g.FillRectangle(Brush, xPos, yPos, xPos + width, yPos + height);
            }
        }

        public void DrawTriangle(int x, int y)
        {
           // g.DrawPolygon(Pen, x, y, 3); to do
        }

        public void Clear() //clears canvas
        {
            g.Clear(Color.Empty);
        }

        public void PenReset() //resets pen position
        {
            xPos = yPos = 0;
        }

    }
}

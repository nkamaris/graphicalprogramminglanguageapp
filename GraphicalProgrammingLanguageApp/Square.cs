﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    class Square : Rectangle
    {
        private int size;

        public Square() : base()
        {

        }
        public Square(Color colour,int x, int y, int size):base(colour, x, y, size, size)
        {
            this.size = size;
        }
        public override double calcArea()
        {
            throw new NotImplementedException();
        }

        public override double calcPerimeter()
        {
            throw new NotImplementedException();
        }

        public override void draw(Graphics g)
        {
            base.draw(g);
        }
    }
}
